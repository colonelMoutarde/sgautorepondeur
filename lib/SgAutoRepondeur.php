<?php

/*
 * Copyright (C) 2015 Luc Sanchez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace SgAutoRepondeur;

use \SgAutoRepondeur\SgException\SgException;

/**
 * Description of SgAutoRepondeur
 *
 * @author luc
 */
class SgAutoRepondeur {

    private $inscriptionNormale = 'oui'; // oui = inscription comme si l'abonn� avait lui m�me rempli le formulaire et donc il y a redirection par l'auto-r�pondeur.
    private $membreID;
    private $codeActivationClient;
    private $emailSG;
    private $listeID;
    private $fields;

    public function getListeID() {
        return $this->listeID;
    }

    public function setListeID($listeID) {
        $this->listeID = $listeID;
        return $this;
    }

    public function getInscriptionNormale() {
        return $this->inscriptionNormale;
    }

    public function getMembreID() {
        return $this->membreID;
    }

    public function getCodeActivationClient() {
        return $this->codeActivationClient;
    }

    public function getEmailSG() {
        return $this->emailSG;
    }

    public function setInscriptionNormale($inscriptionNormale) {
        $this->inscriptionNormale = $inscriptionNormale;
        return $this;
    }

    public function setMembreID($membreID) {
        $this->membreID = $membreID;
        return $this;
    }

    public function setCodeActivationClient($codeActivationClient) {
        $this->codeActivationClient = $codeActivationClient;
        return $this;
    }

    public function setEmailSG($emailSG) {

        if (filter_var($emailSG, FILTER_VALIDATE_EMAIL)) {
            $this->emailSG = $emailSG;
            return $this;
        } else {
            throw new SgException(__METHOD__ . '--> Invalid eMail');
        }
    }
    
    /**
     * @name setFields()
     * @param \SgAutoRepondeur\SgAutoRepondeur $credential
     * @param \SgAutoRepondeur\User $user
     * @return \SgAutoRepondeur\SgAutoRepondeur
     */
    public function setFields(\SgAutoRepondeur\SgAutoRepondeur $credential, \SgAutoRepondeur\User $user) {

        $fields = [
            'membreid'             => $credential->getMembreID(),
            'codeactivationclient' => $credential->getCodeActivationClient(),
            'inscription_normale'  => $credential->getInscriptionNormale(),
            'listeid'              => $credential->getListeID(),
            'email'                => $user->getEmail(),
            'nom'                  => $user->getNom(),
            'prenom'               => $user->getPrenom(),
            'civilite'             => $user->getCivilite(),
            'adresse'              => $user->getAdresse(),
            'codepostal'           => $user->getCodePostal(),
            'ville'                => $user->getVille(),
            'pays'                 => $user->getPays(),
            'siteweb'              => $user->getSiteweb(),
            'telephone'            => $user->getTelephone(),
            'parrain'              => $user->getParrain(),
            'fax'                  => $user->getFax(),
            'msn'                  => $user->getMsn(),
            'skype'                => $user->getSkype(),
            'pseudo'               => $user->getPseudo(),
            'sexe'                 => $user->getSexe(),
            'journaissance'        => $user->getJournaissance(),
            'moisnaissance'        => $user->getMoisNaissance(),
            'anneenaissance'       => $user->getAnneeNaissance(),
            'ip'                   => $user->getIp(),
            'identite'             => $user->getIdentite(),
            'redirect'             => $user->getRedirect(),
            'redirect_onlist'      => $user->getRedirectOnlist(),
            'champs_1'             => $user->getChamps1(),
            'champs_2'             => $user->getChamps2(),
            'champs_3'             => $user->getChamps3(),
            'champs_4'             => $user->getChamps4(),
            'champs_5'             => $user->getChamps5(),
            'champs_6'             => $user->getChamps6(),
            'champs_7'             => $user->getChamps7(),
            'champs_8'             => $user->getChamps8(),
            'champs_9'             => $user->getChamps9(),
            'champs_10'            => $user->getChamps10(),
            'champs_11'            => $user->getChamps11(),
            'champs_12'            => $user->getChamps12(),
            'champs_13'            => $user->getChamps13(),
            'champs_14'            => $user->getChamps14(),
            'champs_15'            => $user->getChamps15(),
            'champs_16'            => $user->getChamps16(),
        ];

        $this->fields = $fields;
        return $this;
    }

    public function getFields() {
        return $this->fields;
    }

    public function callWebService($query) {

        return $this->header(http_build_query($query));
    }

    /**
     * @param $query
     * @return string
     */
    private function header($query) {
        $inBody  = false;
        $body    = [];

        $fp = fsockopen('sg-autorepondeur.com', 80);

        fwrite($fp, "POST /inscr_decrypt.php HTTP/1.1\r\n");
        fwrite($fp, "Host: sg-autorepondeur.com\r\n");
        fwrite($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
        fwrite($fp, "Content-Length: " . strlen($query) . "\r\n");
        fwrite($fp, "Connection: close\r\n");
        fwrite($fp, "\r\n");

        fwrite($fp, $query);

        while (!feof($fp)) {
            if (!$inBody) {
                // Read HTTP headers
                $line = trim(fgets($fp, 1024));
                if ($line === null) {
                    $inBody = true;
                    continue;
                }
            } else {
                // Read HTTP body
                $body[] = fgets($fp, 1024);
            } 
        }
        
        // Success means: doesn't return anything at all in the body
        $bodyContent = trim(implode(null, $body));
        //Cette ligne facultative sert � v�rifier le code retourn� par SG AUtor�pondeur ou � rediriger l'abonn� en fonction de la variable $inscription_normale
         return $bodyContent;
    }

}
