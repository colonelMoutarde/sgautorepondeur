<?php

/*
 * Copyright (C) 2015 Luc Sanchez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace SgAutoRepondeur\ResponseDictionary;

use SgAutoRepondeur\SgException\SgException;

class ResponseDictionary {
    
    /**
     * 
     * @param string $value
     * @throws SgException
     */
    public function value($value) {

        switch ($value) {
            case 'informationmanquante':
                throw new SgException('il manque des champs obligatoires.', 1);
                break;
            case 'emailexistant':
                throw new SgException('L\'email existe déjà dans la liste', 2);
                break;
            case 'paysblackliste':
                throw new SgException('Le pays a été bloqué', 3);
                break;
            case 'nombreipimportant':
                throw new SgException('Trop d\'inscriptions avec la même adresse IP', 4);
                break;
            case 'nouvellelisteok':
                throw new SgException('Inscription OK suite à une segmentation comportementale', 5);
                break;
            case 'demandeconfirmation':
                throw new SgException('Demande de confirmation envoyée (double optin)', 6);
                break;
            case 'inscriptionok':
                throw new SgException('L\'inscription est enregistrée (simple optin)',7);
                break;
            case 'mailformatincorrect':
                throw new SgException('L\'email n\'est pas au bon format',8);
                break;
            case 'accesinterdit':
                throw new SgException('Erreur sur une des variables $membreid ou $listeid ou $codeactivationclient',9);
                break;
            default:
                return 'ok';
                break;
        }
    }

}
