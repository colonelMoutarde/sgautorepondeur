<?php

/*
 * Copyright (C) 2015 Luc Sanchez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace SgAutoRepondeur\User;

use SgAutoRepondeur\SgException\SgException;

/**
 * Description of User
 *
 * @author luc
 */
class User {

    private $email;
    private $nom;
    private $prenom;
    private $civilite;
    private $adresse;
    private $codePostal;
    private $ville;
    private $pays;
    private $siteWeb;
    private $telephone;
    private $parrain;
    private $fax;
    private $msn;
    private $skype;
    private $pseudo;
    private $sexe;
    private $jourNaissance;
    private $moisNaissance;
    private $anneeNaissance;
    private $ip;
    private $identite;
    private $redirect; /* url of redirection */
    private $redirectOnlist; /* url of redirection if already a member */
    private $champs1;
    private $champs2;
    private $champs3;
    private $champs4;
    private $champs5;
    private $champs6;
    private $champs7;
    private $champs8;
    private $champs9;
    private $champs10;
    private $champs11;
    private $champs12;
    private $champs13;
    private $champs14;
    private $champs15;
    private $champs16;

    public function getEmail() {
        return $this->email;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }

    public function getCivilite() {
        return $this->civilite;
    }

    public function getAdresse() {
        return $this->adresse;
    }

    public function getCodePostal() {
        return $this->codePostal;
    }

    public function getVille() {
        return $this->ville;
    }

    public function getPays() {
        return $this->pays;
    }

    public function getSiteWeb() {
        return $this->siteWeb;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    public function getParrain() {
        return $this->parrain;
    }

    public function getFax() {
        return $this->fax;
    }

    public function getMsn() {
        return $this->msn;
    }

    public function getSkype() {
        return $this->skype;
    }

    public function getPseudo() {
        return $this->pseudo;
    }

    public function getSexe() {
        return $this->sexe;
    }

    public function getJourNaissance() {
        return $this->jourNaissance;
    }

    public function getMoisNaissance() {
        return $this->moisNaissance;
    }

    public function getAnneeNaissance() {
        return $this->anneeNaissance;
    }

    public function getIp() {
        return $this->ip;
    }

    public function getIdentite() {
        return $this->identite;
    }

    public function getRedirect() {
        return $this->redirect;
    }

    public function getRedirectOnlist() {
        return $this->redirectOnlist;
    }

    public function getChamps1() {
        return $this->champs1;
    }

    public function getChamps2() {
        return $this->champs2;
    }

    public function getChamps3() {
        return $this->champs3;
    }

    public function getChamps4() {
        return $this->champs4;
    }

    public function getChamps5() {
        return $this->champs5;
    }

    public function getChamps6() {
        return $this->champs6;
    }

    /**
     * @return mixed
     */
    public function getChamps7() {
        return $this->champs7;
    }

    /**
     * @return mixed
     */
    public function getChamps8() {
        return $this->champs8;
    }

    public function getChamps9() {
        return $this->champs9;
    }

    public function getChamps10() {
        return $this->champs10;
    }

    public function getChamps11() {
        return $this->champs11;
    }

    /**
     * @return mixed
     */
    public function getChamps12() {
        return $this->champs12;
    }

    public function getChamps13() {
        return $this->champs13;
    }

    public function getChamps14() {
        return $this->champs14;
    }

    public function getChamps15() {
        return $this->champs15;
    }

    public function getChamps16() {
        return $this->champs16;
    }

    public function setEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
            return $this;
        } else {
            throw new SgException(__METHOD__ . ' --> Invalid eMail');
        }
    }

    public function setNom($nom) {
        $this->nom = $nom;
        return $this;
    }

    public function setPrenom($prenom) {
        $this->prenom = $prenom;
        return $this;
    }

    public function setCivilite($civilite) {
        $this->civilite = $civilite;
        return $this;
    }

    public function setAdresse($adresse) {
        $this->adresse = $adresse;
        return $this;
    }

    public function setCodePostal($codePostal) {
        $this->codePostal = $codePostal;
        return $this;
    }

    public function setVille($ville) {
        $this->ville = $ville;
        return $this;
    }

    public function setPays($pays) {
        $this->pays = $pays;
        return $this;
    }

    public function setSiteWeb($siteWeb) {
        $this->siteWeb = $siteWeb;
        return $this;
    }

    public function setTelephone($telephone) {
        $this->telephone = $telephone;
        return $this;
    }

    public function setParrain($parrain) {
        $this->parrain = $parrain;
        return $this;
    }

    public function setFax($fax) {
        $this->fax = $fax;
        return $this;
    }

    public function setMsn($msn) {
        $this->msn = $msn;
        return $this;
    }

    public function setSkype($skype) {
        $this->skype = $skype;
        return $this;
    }

    public function setPseudo($pseudo) {
        $this->pseudo = $pseudo;
        return $this;
    }

    public function setSexe($sexe) {
        $this->sexe = $sexe;
        return $this;
    }

    public function setJourNaissance($jourNaissance) {
        $this->jourNaissance = $jourNaissance;
        return $this;
    }

    public function setMoisNaissance($moisNaissance) {
        $this->moisNaissance = $moisNaissance;
        return $this;
    }

    public function setAnneeNaissance($anneeNaissance) {
        $this->anneeNaissance = $anneeNaissance;
        return $this;
    }

    public function setIp($ip) {

        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            $this->ip = $ip;
            return $this;
        } else {
            throw new SgException(__METHOD__ . ' --> Invalide IP');
        }
    }

    public function setIdentite($identite) {
        $this->identite = $identite;
        return $this;
    }

    public function setRedirect($redirect) {
        $this->redirect = $redirect;
        return $this;
    }

    public function setRedirectOnlist($redirectOnlist) {
        $this->redirectOnlist = $redirectOnlist;
        return $this;
    }

    public function setChamps1($champs1) {
        $this->champs1 = $champs1;
        return $this;
    }

    public function setChamps2($champs2) {
        $this->champs2 = $champs2;
        return $this;
    }

    public function setChamps3($champs3) {
        $this->champs3 = $champs3;
        return $this;
    }

    public function setChamps4($champs4) {
        $this->champs4 = $champs4;
        return $this;
    }

    public function setChamps5($champs5) {
        $this->champs5 = $champs5;
        return $this;
    }

    public function setChamps6($champs6) {
        $this->champs6 = $champs6;
        return $this;
    }

    public function setChamps7($champs7) {
        $this->champs7 = $champs7;
        return $this;
    }

    public function setChamps8($champs8) {
        $this->champs8 = $champs8;
        return $this;
    }

    public function setChamps9($champs9) {
        $this->champs9 = $champs9;
        return $this;
    }

    public function setChamps10($champs10) {
        $this->champs10 = $champs10;
        return $this;
    }

    public function setChamps11($champs11) {
        $this->champs11 = $champs11;
        return $this;
    }

    public function setChamps12($champs12) {
        $this->champs12 = $champs12;
        return $this;
    }

    public function setChamps13($champs13) {
        $this->champs13 = $champs13;
        return $this;
    }

    public function setChamps14($champs14) {
        $this->champs14 = $champs14;
        return $this;
    }

    public function setChamps15($champs15) {
        $this->champs15 = $champs15;
        return $this;
    }

    public function setChamps16($champs16) {
        $this->champs16 = $champs16;
        return $this;
    }

}
