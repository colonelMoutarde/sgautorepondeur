##Usage
If you have a question contact sgautorepondeur.com

```php

<?php

require_once 'vendor/autoload.php';

try {
    $monTest = (new \SgAutoRepondeur\SgAutoRepondeur())
            ->setCodeActivationClient('1010201000162121718617151918911')
            ->setMembreID('32')
            ->setInscriptionNormale('non')
            ->setEmailSG('sebastienXXX_@hotmail.com')
            ->setListeID('779')

    ;
} catch (SgAutoRepondeur\SgException\SgException $exc) {
    echo $exc->getMessage();
}

try {
    $userSG = (new \SgAutoRepondeur\User\User())
            ->setEmail('tutu@tutu.fr')
            ->setNom('Dupont')
            ->setPrenom('Jean')
            ->setAdresse('125 rue de nulle part')
            ->setPays('France')
            ->setAnneeNaissance('1979')
            ->setMoisNaissance('01')
            ->setJourNaissance('10')
            ->setVille('lyon')
            ->setIp('192.168.1.236');
    
    $monTest->setFields($monTest, $userSG);

  //  var_dump($monTest->getFields());
} catch (SgAutoRepondeur\SgException\SgException $exc) {
    echo $exc->getMessage();
}

try {
    var_dump( $result = (new SgAutoRepondeur\ResponseDictionary\ResponseDictionary())
        ->value( $monTest->callWebService($monTest->getFields()) ));
} catch (SgAutoRepondeur\SgException\SgException $exc) {
    echo $exc->getMessage();
}


```
